---
layout: page
title: Föreningen
section_id: organization
---

# Styrelse och mentorer

- Tobias Ritzau, ordförande
- Stefan Bäckstrand, kassör
- Johan Stjernbecker, revisor
- Christel Andersson, ledamot
- Joakim Andersson, ledamot
- Linnéa Mörk, ledamot
- Helene Rignér, ledamot
- Stefan Fernryd, suppleant
- Stefan Lindsjö, valberedning
- Rafaa Turki, valberedning
- Jon Blomstedt, mentor
- Henrik Larsson, mentor
- Fredrik Sivermark, mentor


## Dokument

- [Protokoll från 2018 års årsmöte]({{ site.baseurl }}{% post_url 2019-05-22-protokoll-arsmote %})
- [Protokoll från styrelsemöte 2019-06-26]({{ site.baseurl }}{% post_url 2019-06-26-protokoll-styrelsemote %})

<div class="four spacing"></div>