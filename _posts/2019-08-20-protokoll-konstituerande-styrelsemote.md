---
layout: post
title:  "Protokoll konstituerande styrelsemöte"
date:   2019-08-20 08:00:00
categories: [update, doc]
---

## Konstituerande styrelsemöte - CoderDojo Veberöd

- Datum: 2019-08-20
- Närvarande: Tobias Ritzau (ordförande), Stefan Bäckstrand (kassör), Balázs Suhajda, 
  Joakim Andersson, Rafaa Turki
- Plats: Ängavägen 36B - Hos Tobias

1. Val av justeringsperson:
   Att justera dagens protokoll utses Rafaa Turki. Ordförande är ständigt justerare 
   och skriver på samtliga protokoll.

2. Tecknande av föreningens firma: Föreningens firma tecknas av: Tobias Ritzau 
   (710317-4210) samt Stefan Bäckstrand (810414-0333) i förening.

3. Övriga frågor: Inga övriga frågor fanns att diskutera, ordföranden avslutar
   mötet och tackar för visat intresse.


| <br><br><br>____________________________________________________________________________________________ | <br><br><br>____________________________________________________________________________________________ |
|:--------------------------------|:--------------------------------|
| Tobias Ritzau<br>Ordförande     | Rafaa Turki<br>Justeringsperson |
