---
layout: post
title:  "Protokoll styrelsemöte"
date:   2019-06-26 08:00:00
categories: [update, doc]
---

## Styrelsemöte - CoderDojo Veberöd

- Datum: 2019-06-26
- Närvarande: Christel Andersson, Stefan Bäckstrand, Linnéa Mörk och Tobias Ritzau
- Plats: Hos Tobias


### Formalia

- Överlämning från förra styrelsen: Vi bokar ett separat möte för det så fort som möjligt.
  - [x] Tobias bokar möte.
- GDPR: För att undvika problem så gör hanterar vi personuppgifter så enkelt 
  och analogt som möjligt.
  - Lagra medlemsregister lokalt på Tobias och Stefans datorer.
  - Skriva ut om flera personer behöver det.
  - Vill vi så kan det vara ok att kryptera och då lagra på typ Dropbox.
  - Om information om till exempel allergier och liknande behövs så måste den
    förstöras så fort som möjligt.


### Sommarevent

Vi har inget, istället kommer vi att starta terminen med ett öppet hus en helgdag 
någon vecka in i terminen. Vi bestämde också att vi kommer avsluta terminen med 
ett liknande event där höstens arbete presenteras.

 
### CoolMinds Big bang festival

Vi kan inte på grund av semester.
- [x] Tobias meddelar CoolMinds.
 

### Föreningsdagen

- Föreningarnas dag är den 17 augusti på badet.
- Plan A är att Stefan skall prata med förskolan om en robotsak som de använt.
- Vi skall fundera vidare på om vad som skall hända när man "vinner"
- Tobias har pratat med Jan Malmberg från unga unga smarta byar. Eventuellt kan 
  vi starta ett samarbete. De skall också vara med på dagen. Vi planerar att 
  träffas ganska snart för att snacka ihop oss. Vem vill vara med?
  - [ ] Tobias bokar möte.


### Hur förbereder vi inför hösten?

- Vi börjar några veckor efter skolstart med ett "öppethus" (se ovan).
- Vi informerar med reklamblad på skolan, via Facebook och eventuellt genom
  att gå ut och presentera oss i skolan.
- Vi organiserar verksamheten i två grupper: en för de som behöver mer av en
  mjukstart och en mer avancerad. 
- Vi behöver någon som tar tag i de båda grupperna och lägger upp en planering.
  - Tobias anmäler sig frivilligt till den mer avancerade gruppen. Vem hänger på?
  - Vem tar tag i de andra?
  - Vad kallar vi grupperna?
- På årsmötet beslutade vi att köra varannan vecka på torsdagar i två parallella grupper.
- Under mötet diskuterade vi olika idéer på vad vi skulle kunna hitta på:
  - Leekwars (purjulök med pistol-robot? Linnéa vet mer.
  - Någon c# sak med tanks som Stefan kan berätta mer om.
  - Minecraft
  - Scratch
  - Gärna typ ett showroom på sista gången då ma kan visa upp det man har gjort.
  - Comando lekar, robotarna från förskolan göra banor till andra.
  - Det är bra om vi kör samma ämne två gånger i följd så att man hänger med även
    om man missar enstaka tillfällen.
  
- Tar någon huvudansvar för grupperna?
  - [x] Tobias skriver i messengergruppen att vi behöver någon för de yngre och 
    någon för de äldre.


### Övrigt

- Vi har blivit erbjudna GitLabs största abonnemang gratis. Det får endast användas 
  till utbildning och ser vi att något projekt växer utöver det så skall det flyttas
  ur vårt abonnemang.
- Vi kommer även ha vår hemsida där med information så som medlemsavgift, betalning, 
  protokoll, kodexempel, ...