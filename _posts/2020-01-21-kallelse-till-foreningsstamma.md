---
layout: post
title:  "Kallelse till föreningsstämma"
date:   2020-01-21 20:30:00
categories: [update]
---

Kallelse till föreningsstämma i CoderDojo Veberöd
=================================================

Datum: Torsdag 2020-02-06

Tid: 18.30-19.30

Plats: Fritidsgården


Agenda
------

1.  Val av ordförande vid föreningsstämman och anmälan av 
    stämmoordförandens val av protokollförare.

2.  Godkännande av röstlängden.

    _Förslag: Styrelse, mentorer och betalande medlemmar._

3.  Val av en eller två justerare.

4.  Frågan om föreningsstämman blivit utlyst i behörig ordning.

5.  Fastställande av dagordningen.

6.  Styrelsens årsredovisning och revisionsberättelsen.

7.  Beslut om fastställande av resultaträkningen och balansräkningen
    samt om hur vinsten eller förlusten enligt den fastställda
    balansräkningen ska disponeras.

    _Förslag: Pengarna står kvar._

8.  Beslut om ansvarsfrihet åt styrelseledamöterna.

9.  Frågan om arvoden till styrelseledamöterna och revisorerna.

    _Förslag: 0 kr_

10. Medlemsavgift för kommande verksamhetsår.

    _Förslag: Oförändrad (200 kr per år med 90 % rabatt för styrelse 
      och mentorer.)_

11. Beslut om antalet styrelseledamöter och styrelsesuppleanter som ska
    väljas.

    _Förslag: 6 ordinarie styrelseledamöter och inga suppleanter._

12. Val av styrelseledamöter och eventuella styrelsesuppleanter.

    _Förslag:_
    - Christel Andersson
    - Jon Bolmstedt
    - Stefan Fernryd
    - Henrik Larsson
    - Linnéa Mörk
    - Tobias Ritzau

13. Val av revisorer och eventuella revisorssuppleanter.

    _Förslag: Johan Stjernbecker_

14. Val av valberedning.

15. Fråga om förändring av stadgar enligt nedan.

---

### § 9 Styrelse

__Nuvarande:__ ~~Styrelsen ska bestå av lägst tre och högst 6 
styrelseledamöter med lägst 1 och högst 4 styrelsesuppleanter. 
Styrelseledamöter och styrelsesuppleanter väljs av föreningsstämman för 
tiden fram till slutet av nästa ordinarie föreningsstämma.~~

__Förslag:__ Styrelsen ska bestå av lägst fyra och högst tio 
styrelsemedlemmar varav högst sex är ordinarie styrelseledamöter och de 
andra är styrelsesuppleanter. Styrelseledamöter och styrelsesuppleanter 
väljs av föreningsstämman. Styrelsemedlemmarna väljs för tiden fram till 
slutet av den ordinarie föreningsstämma två verksamhets år senare. 
Undantaget ordförande som väljs på ett år fram till slutet av nästa 
ordinarie föreningsstämma. Styrelsen kan även göra undantag med målet 
att endast halva styrelsen väljs om vid varje föreningsstämma.

### § 11 Verksamhets- och räkenskapsår

__Nuvarande:__ ~~Föreningens verksamhets- och räkenskapsår är 15 Augusti
– 14 Augusti.~~

__Förslag:__ Föreningens verksamhets- och räkenskapsår är 1 augusti – 31 
juli.
