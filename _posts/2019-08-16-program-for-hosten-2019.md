---
layout: post
title:  "Progam för hösten 2019"
date:   2019-08-16 08:19:12
categories: update
---

Vi kommer att inleda hösten med ett öppet hus på Veberöds Scoutkår lördagen den
31/8 klockan 14-17. Dit är alla som är intresserade av vår verksamhet
välkomna. Programmet är inte satt än men vi planerar att visa lite:

- Programmeringsspel
- Scratch
- HTML/CSS för att göra hemsidor
- Python

Därefter kommer höstens verksamhet dra igång. Vi kommer att köra varannan
torsdag med start 12/9 i Fritid Veberöds lokaler. För att få bärighet i
verksamheten kommer vi att ta ut en medlemsavgift på 200 kr/år.

Deltagarna kommer att delas in i två grupper: *n00bs* och *haxx0rs*. Våra n00bs
kommer att fokusera på programmeringsspel, Scratch och liknande verktyg, medans
våra haxx0rs kommer att jobba med Python. De kommer att köras parallellt med
start 18.30. Våra n00bs avslutar 20.00 och våra haxx0rs fortsätter till 20.30.

Man behöver en dator för att delta i verksamheten. Vi har ett antal datorer
till utlåning. Prata med oss så försöker vi se till så att alla kan vara med.
Medlemsskap är heller inget strikt krav för att delta.

Välkomna!
